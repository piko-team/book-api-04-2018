const assert = require('assert');
const bookControllerFactory = require('../../src/bookController');

describe('Book controller', function(){
    it('should details ', async function(){
        const bookRepository = {
            getDetails(isbn){
                bookRepository.getDetails.invokedWith = isbn;
                return Promise.resolve('some book with isbn 1234567890');
            }
        };
        const bookController = bookControllerFactory(bookRepository);
        const req = {
            params: {
                isbn: '1234567890'
            }
        };        
        const res = {
            json(book) {
                res.json.invokedWith = book;
            }
        };
        await bookController.details(req, res);

        assert.equal(bookRepository.getDetails.invokedWith, '1234567890');
        assert.equal(res.json.invokedWith, 'some book with isbn 1234567890');
    });

    it('should create new book', async function(){
        const bookRepository = {
            createOrUpdate(title, authors, isbn, description){
                bookRepository.createOrUpdate.isInvoked = true;
                return Promise.resolve('book created or updated');
            }
        };
        const bookController = bookControllerFactory(bookRepository);
        const req = {
            body: {
                title: 'The man from high castle', 
                authors: 'Dick',
                description: 'The Dick\'s book',
                isbn: '1234567890'
            }
        };        
        const res = {
            redirect(url){
                res.redirect.isInvoked = true;
            }
        };

        await bookController.createOrUpdate(req, res);

        assert.ok(bookRepository.createOrUpdate.isInvoked);
        assert.ok(res.redirect.isInvoked);
    });

    it('should fail createOrUpdate when DB is down', async function(){
        const bookRepository = {
            createOrUpdate(title, authors, isbn, description){
                return Promise.reject(new Error('Permanent context is down'));
            }
        };
        const bookController = bookControllerFactory(bookRepository);
        const req = {
            body: {}
        };        
        const res = {};
        const next = function next(e){ next.invokedWith = e; };

        await bookController.createOrUpdate(req, res, next);

        assert.ok(next.invokedWith, 'Permanent context is down');
    });
});