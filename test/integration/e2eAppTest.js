const httpClient = require('supertest');
const assert = require('assert');
const app = require('../../src/app');

describe('Book inventory', function () {
    it('allows to stock up the items', async function () {
        const request = httpClient(app);
        const createResponse = await request
            .post('/books')
            .send({
                title: "JavaScript in Action",
                authors: ["James Smith", "Kate Donovan"],
                isbn: "0123456789",
                description: "The ultimate JS book!"
            })
            .set('Content-Type', 'application/json')
            .expect(302);

        const readResult = await request
            .get(createResponse.header.location)
            .expect(200);

        assert.deepEqual(readResult.body, {
            title: "JavaScript in Action",
            authors: ["James Smith", "Kate Donovan"],
            isbn: "0123456789",
            description: "The ultimate JS book!"
        });
    })
});