function isProduction(){
    return process.env.NODE_ENV === 'production';
}

module.exports = {
    clientError: function clientError(req, res, next) {
        const err = new Error("Not Found Buddy!");
        err.status = 404;
        next(err);
    },
    serverError: function serverError(err, req, res, next) {
        console.error(err.stack);
        res.status(500);
        res.json({message: err.message, error: isProduction() ? {} : err.stack});
    }
};