const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/booksapi';

let booksPromise = MongoClient.connect(url, {bufferMaxEntries: 0}).then(function (client) {
    return client.db().collection("books");
});

async function createOrUpdate(title, authors, isbn, description){
    const books = await booksPromise;
    await books.updateOne(
        {isbn: isbn},
        {$set: {title, authors, isbn, description}},
        {upsert: true}
    );
}

async function getDetails(isbn){
    const books = await booksPromise;
    return await books.findOne(
        {isbn},
        {projection: {_id: 0}}
    );
}

module.exports = {
    createOrUpdate,
    getDetails
};