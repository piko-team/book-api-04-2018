const router = require('express').Router();
const bookRepository = require('./bookRepository');
const bookController = require('./bookController')(bookRepository);

router.post("/", bookController.createOrUpdate);
router.get("/:isbn", bookController.details);

module.exports = router;