module.exports = function bookControllerFactory(bookRepository) {
    return {
        createOrUpdate: async function (req, res, next) {
            const {title, authors, isbn, description} = req.body;
            try {
                await bookRepository.createOrUpdate(title, authors, isbn, description);
                res.redirect(`/books/${isbn}`);
            } catch(e) {
                next(e);
            }
        },
        details: async function (req, res, next) {
            const isbn = req.params.isbn;
        
            try {
                res.json(await bookRepository.getDetails(isbn));
            } catch(e){
                next(e);
            }
        }
    };    
};