const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const bookRoutes = require('./bookRoutes');
const errors = require('./errors');

app.use(function logRequests(req, res, next) {
    console.log('new request at ' + new Date());
    next();
});

function authorization(req, res, next) {
    console.log('authorizing this path ' + req.path);
    next(new Error("oh noooooooo!"));
}

app.use(bodyParser.json());

app.use("/books", bookRoutes);

app.use(errors.clientError);
app.use(errors.serverError);

module.exports = app;