## book-api

### hello world

* Stwórz katalog book-api
* W tym katalogu tworzymy podkatalogi src i test
* W src tworzymy plik server.js

```javascript
console.log("Hello world");
```

Odpalamy nasz pierwszy skrypt
```
node src/server.js
```

### hello server

Czas na pierwszy Hello Server
```javascript
const http = require("http");

const server = http.createServer(function (req, res) {
    res.end("hello world");
});

server.listen(3000, function () {
    console.log("Listening on port: " + 3000);
});
```

* require - wsparcie dla modułów w Node.js (CommonJS)
* http - niskopoziomowy moduł wbudowany w Node.js
* http.createServer - API z callbackami 
* dla każdego requesta wywołaj funkcję
* server.listen - ponownie API z callbackami - zacznij nasłuchiwać 

### run to completion 

```javascript
let counter = 0;
const server = http.createServer(function (req, res) {
    counter++;
    res.end("hello world " + counter);
});
```

Nie potrzeba lock'ów na zmienną counter. Mamy gwarancję, że tylko 
jeden wątek jest w danej chwili wewnątrz funkcji obsługującej zapytanie.
Node.js nadaje się bardziej do aplikacji ograniczonych przez I/O (np. CRUD, orkiestracja), nie przez CPU (np. procesowanie
gigantycznych XML, skomplikowane obliczenia).
Dlaczego? Operacje I/O odbywają się poza głównym wątkiem, operacje takie jak obliczenia
numeryczne zajmują główny wątek.

### express.js

Praca z niskopoziomowymi modułami wbudowanymi w Node.js wymaga
pisania dużej ilości kodu boilerplate. 
Skorzystajmy zatem z biblioteki wysokopoziomowej express.js, która
może slużyć do pisania API oraz aplikacji webowych. 
Ponieważ express.js nie jest częścią core Node.js musimy dodać
bibliotekę jako zewnętrzną zależność. Tu z pomocą przychodzi narzędzie npm.

### package.json

Aby dodawać moduły z użyciem npm potrzebujemy plik package.json. 
W katalogu book-api stwórz plik package.json z pustym obiektem {}.
Następnie wywołaj komendę:
```
npm i express
```
lub dłuższą wersję tej samej komendy
```
npm install express
```
Po wykonaniu komendy w pliku package.json znajduje się nasza zależność:
```
{
  "dependencies": {
    "express": "4.15.4"
  }
}
```
Jeżeli nie chcesz mieć ruchomych wersji zależności:
```
npm config set save-exact true 
```

Po zainstalowaniu powinien się otworzyć katalog node_modules, zawierający
express.js i wszystkie jego tranzytywne zależności.
Jeżeli używasz git'a dodaj ten katalog do .gitignore

### hello express.js

W server.js zastąp istniejący kod przez:
```javascript
const express = require("express");
const app = express();

app.get("/", function (req, res) {
    res.send("Hello World!");
});

app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});
```

Co robi express.js?
* mapuje świat HTTP np. GET / na wywołania funkcji

Funkcja send domyśnie zwraca text/html

### middleware

Kluczową koncepcją na której bazuje express jest tzw. middleware.
Middleware to funkcja która ma następującą sygnaturę:
```javascript
function middleware(req, res, next) {
    
}
```

W poniższym przykładzie chcemy logować każdy request w naszej aplikacji:
```javascript
app.use(function(req, res, next) {
    console.log("new request at " + new Date());
    next();
});

app.get("/", function (req, res) {
    res.send("Hello World!");
});
```
app.use() wpina middleware do logowania przed naszym request handlerem.
Kolejność middleware ma znaczenie. Middleware puszcza request dalej
jeżeli wywołamy funkcję next().

Middleware można łączyć w łańcuch wywołań:
```javascript
app.use(function(req, res, next) {
    console.log("new reqest at " + new Date());
    next();
});

app.use(function(req, res, next) {
    console.log("doing authorization");
    next();
});

app.get("/", function (req, res) {
    res.send("Hello World!");
});
```
Każdy middleware powinien obsługiwać jedną funkcjonalność typu
cross-cutting np:
* logowanie requestów
* zapis audit log
* ustawianie ciasteczek

### middleware i obsługa błędów

express.js ma swoją domyślną obsługę błędów.
Aby spreparować błąd możemy:
* rzucić wyjątkiem w middleware lub request handlerze
* przekazać do funkcji next() wartość np. next("error")  lub next(new Error("error message"))

### własna obsługa błędów

Aby samemu obsłużyć błędy dodajemy na końcu listy (po middleware i handlerach)

```javascript
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.json({message: err.message, error: err.stack});
});
```

Jest to specjalny 4-argumentowy middleware do obsługi błędów.
Chccemy zalogować stacktrace, ustawić kod odpowiedzi na 500
i odesłać JSON z polami message oraz error.

Błąd możemy spreparować w dowolnym middleware lub handlerze
```
next(new Error("custom error"));
```
lub
```javascript
throw new Error("custom error");
```

### 404 

Jak obsłużyc błędy 404?

```javascript
app.get("/", function (req, res) {
    res.send("Hello World!");
});

app.use(function (req, res, next) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(err.status || 500); // zmieniona linijka
    res.json({message: err.message, error: err.stack});
});
```
Dodajemy middleware za ostatnim handlerem.
Jeżeli żaden z handlerów nie obsłużył requestów (robiąc res.send lub res.json)
to nasz request automatycznie wpadnie w nasz nowy middleware.
Tutaj ustawiamy własny błąd 404 i puszczay dalej do generycznego
handlera. 

### npm scripts

Zamiast odpalać wszystko komendą node src/server.js napiszmy skrypt który
ukryje nazwę pliku.

W package.json
```
{
  "scripts": {
    "start": "node src/server.js"
  },
  "dependencies": {
    "express": "4.15.4"
  }
}
```
Odpalamy jako
```
npm run start
```
albo skrótem (kilka popularnych komend ma wersję skróconą bez run)
```
npm start
```

### nodemon

Aby dodać autoreload przy zmianach pliku:
```
npm i nodemon -D
```
I komenda watch:
```
{
  "scripts": {
    "start": "node src/server.js",
    "watch": "nodemon src/server.js"
  },
  "dependencies": {
    "express": "4.15.4"
  },
  "devDependencies": {
    "nodemon": "1.12.1"
  }
}
```

Odpalamy jako
```
npm run watch
```

devDependencies to zależności używane tylko do developmentu.

Hint:
Wymuszenie restartu nodemon - rs

### obsługa POST

```
HTTP POST /book
Content-Type: applicaction/json

{ 
  "title": "JavaScript in Action",
  "authors": ["James Smith", "Kate Donovan"],
  "isbn": "0123456789",
  "description": "The ultimate JS book!"
}
```

express.js domyślnie nie parsuje HTTP body
```
npm i body-parser
```

```javascript
const bodyParser = require('body-parser');
app.use(bodyParser.json());
```

Napiszmy echo server który odpowiada tym co dostał:
```javascript
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.post("/book", function(req, res) {
    // destructuring from ES6
    const {title, authors, isbn, description} = req.body;
    res.json({title, authors, isbn, description});
});
```

Testujemy przez curl, postman lub innego klienta HTTP.

### setup mocha

Do wyboru mamy różne test runner (jest, tape, mocha).
```
npm i mocha -D
```

W pliku test/unit/mathTest.js

```javascript
const assert = require('assert');
describe('Math in JS', function () {
    it('should support addition', function () {
        assert.equal(1 + 1, 2);
    });
});
```
Node.js ma wbudowany moduł do asercji. 
Mocha daje nam funkcje describe i it. 

Dodajemy w package.json:
```
"test": "mocha test/unit"
```
Komenda mocha jest dostępna z tzw. npm path (node_modules/.bin)

```
npm test
```

### testy asynchroniczne

```javascript
const  assert = require('assert');

describe('Math in JS', function () {
    it('should support addition', function (done) {
        setTimeout(function () {
            assert.equal(1 + 1, 2);
            done();
        }, 100);
    });
});
``` 
Token done informuje że test się zakończył

### testy integracyjne całej aplikacji

Korzystamy z biblioteki supertest

```
npm i supertest -D
```
https://github.com/visionmedia/supertest

W pliku integration/e2eAppTest.js tworzymy szkielet na podstawie
dokumentacji supertest:
```
const httpClient = require('supertest');
const assert = require('assert');
const app = require('../../src/server');

describe('Book inventory', function () {
    it('supports CRUD lifecycle', function (done) {
        httpClient(app)...

    })
});
```
Nowością jest require który odnosi się do systemu plików.
Idziemy dwa katalogi do góry a później do src/server.js.
Rozszerzenie .js jest opcjonalne. 

Biblioteka supertest oczekuje, że przekażemy instancję aplikacj
express.js. Następnie startuje naszą aplikację na losowym porcie.
Niestety nasza aplikacja w server.js jest na stałe przypisana
do portu 3000. Rozbijmy zatem server.js na dwie części:
* aplikację
* nasłuchiwanie na porcie 3000

server.js
```javascript
const app = require("./app");

app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});
```

app.js
```
const express = require("express");
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.post(...);
....

module.exports = app; // nowa linijka
```
Na końcu app.js eksportujemy obiekt app.

Teraz możemy dokończyć nasz test E2E
```javascript
const httpClient = require('supertest');
const app = require('../../src/app');

describe('Book inventory', function () {
    it('allows to stock up the items', function (done) {
        httpClient(app)
            .post('/book')
            .send({
                title: "JavaScript in Action",
                authors: ["James Smith", "Kate Donovan"],
                isbn: "0123456789",
                description: "The ultimate JS book!"
            })
            .set('Content-Type', 'application/json')
            .expect(200, {
                title: "JavaScript in Action",
                authors: ["James Smith", "Kate Donovan"],
                isbn: "0123456789",
                description: "The ultimate JS book!"
            }, done);
    })
});
```
Co robi ten kod?
* wysyła HTTP POST z odpowiednim body i nagłówkiem HTTP
* sprawdza czy dostaliśmy w odpowiedzi 200 i ten sam content

package.json
```
"test:unit": "mocha test/unit",
"test:integration": "mocha --exit test/integration"
```
Dwa warianty testowe.
--exit przywraca domyślne zachowanie z Mocha 3, tak aby nie trzeba było ręcznie ubijać połączeń do Mongo.

### MongoDB

Baza Mongo pozwala wstawiać do niej całe dokumenty. Zamiast tradycyjnych join'ów mamy kompozycję.
Tranzakcyjność jest tylko na poziomie pojedynczego dokumentu. Baza danych nie wymusza schemy.

```
SQL    -  Mongo
table  -  collection
row    -  document
column -  field
index  -  index
join   -  embedded docs
```

Bazy dokumentowe:
+ dynamiczna schema
+ lepsza wydajność (cały dokument w tym samy miejscu na dysku)
+ bliższe strukturom używanym w kodzie programu
  
Bazy relacyjne:
+ lepsze wsparcie dla join'ów
+ łatwiejsza ewolucja relacji jeden-do-wielu wiele-do-wielu

Jeżeli robimy analitykę zdarzeń łatwiej użyc modelu dokumentów. 
Jeżeli mamy bogaty model z duża liczbą powiązań, być może lepiej użyć bazy relacyjnej. 

Odpalamy bazę komendą
```
mongod
```
Domyślnie serwer bazodanowy startuje na porcie 27017

W razie problemów:
```
mongod --dbpath=/your_db_path
```
Utwórz ścieżkę ręcznie

### MongoDB Node.js driver

```
npm i mongodb 
```

http://mongodb.github.io/node-mongodb-native/3.0/quick-start/quick-start/
Kopiujemy kod z tutoriala
```javascript
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/booksapi';

MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");

    client.close();
});
```

Testujemy połączenie do bazy

### create or update = upsert

Zapiszmy dane do bazy korzystając z funkcji updateOne(selector, new_document, options)
```javascript
app.post("/book", function(req, res) {
    const {title, authors, isbn, description} = req.body;
    MongoClient.connect(url, function(err, client) {
        client.db().collection("books").updateOne(
            {isbn: isbn},
            { $set: {title, authors, isbn, description} },
            {upsert: true}
        );

        client.close();
    });

    res.json({title, authors, isbn, description});
});
```
Używamy isbn jako selektora, następnie wstawiamy nowy dokument do bazy.
W opcjach ustawiamy upsert co pozwala użyć ten sam kod do update'ów.

Używając klienta HTTP przetestujmy czy działa (create i update). 

Weryfikacja z CLI
```
mongo
help
show dbs
use booksapi
show collections
db.books.find()
```

### odczyt danych

```javascript
app.get("/book/:isbn", function (req, res) {
    const isbn = req.params.isbn;
    MongoClient.connect(url, function(err, client) {
        client.db().collection("books").findOne({isbn}, function(err, book) {
            res.json(book);
        });

        client.close();
    });
});
```
Nowy endpoint ma tzw. path parameter. Path param odczytujemy przez req.params. 

Tym razem używamy metody findOne przekazując selector w postaci obiektu z kluczem isbn. 
Operacja bazodanowa jest asynchroniczna więc rejestrujemy callback. 

Testujemy
```
HTTP GET http://localhost:3000/book/2221121314
```

### projekcja

W wyniku dostaliśmy pole _id którego nie chcemy eksponować użytkownikom końcowym.
Dodajmy które pola chcemy wylistować: wszystkie z wyjątkiem _id.

```javascript
client.db().collection("books").findOne({isbn}, { projection: {_id: 0} }, function(err, book) {})
```

### za dużo połączeń

W naszym programie niepotrzebnie nawiązujemy nowe połączenie do bazy przy każdym zapytaniu. 
Połączenie powinno mieć tzw. application scope a nie request scope. 

```javascript
let books;
MongoClient.connect(url, function(err, client) {
    books = client.db().collection("books");
});


app.post("/book", function(req, res) {
    const {title, authors, isbn, description} = req.body;
    books.updateOne(
        {isbn: isbn},
        {$set : {title, authors, isbn, description} },
        {upsert: true}
    );

    res.json({title, authors, isbn, description});
});

app.get("/book/:isbn", function (req, res) {
    const isbn = req.params.isbn;
    books.findOne({isbn}, { projection: {_id: 0} }, function(err, book) {
        res.json(book);
    });
});
```
Co jeżeli nawiązanie połączenia do bazy trwa długo? Zróbmy symulację tej sutyacji:

```javascript
let books;
MongoClient.connect(url, function (err, client) {
    const db = client.db();
    setTimeout(function () {
        collection = db.collection('books');
        books = db.collection("books");
    }, 10000);
});
```

Ja widać praca z callbackami nie jest najłatwiejsza. 

### callbacks -> promises
```
var p = Promise.resolve("db connection");
p.then(function(x) { console.log(x)});
p.then(function(x) { console.log(x)});
p.then(function(x) { console.log(x)});
p.then(function(x) { console.log(x)});
```

Na ratunek przychodzą promisy. 
Promise = pudełko na przyszłą wartość. 

MongoDB Node.js API wspiere i callbacki i Promisy.
```javascript
MongoClient.connect(url, function(err, client) {
    
});
```
```javascript
const promise = MongoClient.connect(url);
promise.then(function(client) {
    
});
promise.catch(function(err) {
    
});
```
Wystarczy, że nie podamy callbacka to dostajemy promise. Promise to jest takie pudełko w którym kiedyś
w przyszłości dostaniemy konkretną wartość. Możemy podpiąć się z funkcją wewnątrz then aby otrzymać przyszłą
wartość, gdy ta będzie dostępna. Możemy też podpiąć się z funkcją wewnątrz catch aby otrzymać notyfikację
w przypadku błędu. 

Tak wygląda zastąpiony kod
```javascript
let booksPromise = MongoClient.connect(url).then(function (client) {
    return client.db().collection("books");
});

app.post("/book", function (req, res) {
    const {title, authors, isbn, description} = req.body;
    booksPromise
        .then(function (books) {
            return books.updateOne(
                {isbn: isbn},
                {$set : {title, authors, isbn, description} },
                {upsert: true}
            );
        })
        .then(function () {
            res.json({title, authors, isbn, description});
        });
});

app.get("/book/:isbn", function (req, res) {
    const isbn = req.params.isbn;
    booksPromise
        .then(function (books) {
            return books.findOne(
                {isbn},
                { projection: {_id: 0} }
            );
        })
        .then(function (book) {
            res.json(book);
        });
});
``` 

### promises i obsługa błędów

Dodaj na końcu łancucha then jeden catch, który obsłuży błędy.
W środku któregokolwiek then'a zasymuluj wyjątek.

### async/await

Najnowsze wersje ES dodają jeszcze jedno udogodnienie aby kod asynchroniczny wyglądał bardziej synchronicznie.

From
```javascript
function promiseBased() {
    promise.then(function(thingInsidePromise) {
    
    });
}
    
```
To
```javascript
async function promiseAndAsyncBased() {
    const thingInsidePromise = await promise;
}
```
From
```javascript
function promiseBased() {
    promise.then(function(thingInsidePromise) {
    
    }).catch(function(e) {
        
    });
}
    
```
To
```javascript
async function promiseAndAsyncBased() {
    try {
        const thingInsidePromise = await promise;
    } catch(e) {
        
    }
}
```

```javascript
app.post("/book", async function (req, res, next) {
    const {title, authors, isbn, description} = req.body;
    try {
        const books = await booksPromise;
        await books.updateOne(
            {isbn: isbn},
            {$set : {title, authors, isbn, description} },
            {upsert: true});
        res.json({title, authors, isbn, description});
    } catch (e) {
        next(e);
    }
});
```

Jako zadanie zastąp drugi endpoint bazując na kodzie powyżej. 

### błędy połączeń do bazy

Zatrzymaj bazę w trakcie działania aplikacji. Zrób zapytanie. Co się stało?

Zmieńmy domyśle zachowanie Mongo:
```
{bufferMaxEntries: 0}
```

### czystsza struktura kodu - osobny plik z routes

Przenieśmy cały routing HTTP do osobnego pliku bookRoutes.js. 
express.js wprowadza koncepcję routera w celu grupowania powiązanych ścieżek routingu.

```javascript
const router = require('express').Router();
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017/booksapi";
let booksPromise = MongoClient.connect(url, {bufferMaxEntries: 0}).then(function (client) {
    return client.db().collection("books");
});

router.post("/", async function (req, res, next) {
    const {title, authors, isbn, description} = req.body;
    try {
        const books = await booksPromise;
        await books.updateOne(
            {isbn: isbn},
            {$set : {title, authors, isbn, description}},
            {upsert: true});
        res.json({title, authors, isbn, description});
    } catch (e) {
        next(e);
    }
});

router.get("/:isbn", async function (req, res, next) {
    try {
        const isbn = req.params.isbn;
        const books = await booksPromise;
        const book =  await books.findOne(
            {isbn},
            { projection: {_id: 0} }
        );
        res.json(book);
    } catch(e) {
        next(e);
    }
});

module.exports = router;
```

Cały routing i potrzebne zależności (od bazy danych) przenosimy do osobnego pliku.
Dodatkowo wyciągamy duplikację w URL (przedrosted /book) i dodajemy go w jednym miejscu w app.js
```javascript
const bookRoutes = require("./bookRoutes");

app.use("/book", bookRoutes); // wpięcie routingu
```

### obsługa błędów w osobnym pliku

error.js
```javascript
module.exports = {
    clientError(req, res, next) {
        const err = new Error("Not Found");
        err.status = 404;
        next(err);
    },
    serverError(err, req, res, next) {
        console.error(err.stack);
        res.status(err.status || 500);
        res.json({message: err.message, error: err.stack});
    }
};
```
I użycie w app.js:
```
const error = require("./error");
...
app.use(error.clientError);
app.use(error.serverError);
```

### obsługa błędów: dev vs prod

W czasie developmentu dobrze mieć dokładny stacktrace przy wyjątkach. 
Natomiast na produkcji nie chcemy zdradzać naszych szczegółów implementacyjnych. 

error.js
```javascript
res.json({message: err.message, error: isProduction() ? {} : err.stack});

function isProduction() {
    return process.env.NODE_ENV === 'production';
}
```

W Node.js używamy zmiennej środowiskowej NODE_ENV aby opisać środowisko które uruchomiliśmy.
process.env udostępnia nam zmienne środowiskowe na poziomie JS.  

Aby nie ustawiać ręcznie tych zmiennych rozbudujmy nasze skrypty npm:
```
    "start": "NODE_ENV=production node src/server.js",
    "dev": "NODE_ENV=development node src/server.js",
```
To rozwiązanie może nie działać w systemie Windows. 

Z pomocą przychodzi narzędzie cross-env
```
npm i cross-env
```

```
"start": "cross-env NODE_ENV=production node src/server.js",
"dev": "cross-env NODE_ENV=development node src/server.js",
```

### routes vs controller

Wróćmy do pliku bookRoutes i wyciągnijmy funkcje obsługujące zapytania do pliku bookController.js
Docelowo w pliku bookRoutes.js chcemy mieć tylko
```javascript
const router = require('express').Router();
const controller = require("./bookController");

router.post("/", controller.createOrUpdate);
router.get("/:isbn", controller.details);

module.exports = router;
```

### repository

Wyciągnijmy teraz kod dostępu do bazy danych do bookRepository.js

```javascript
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017/booksapi";
let booksPromise = MongoClient.connect(url, {
    bufferMaxEntries: 0
}).then(function (client) {
    return client.db().collection("books");
});

module.exports = {
    async createOrUpdate({title, authors, isbn, description}) {
        const books = await booksPromise;
        return await books.updateOne(
            {isbn: isbn},
            {$set : {title, authors, isbn, description} },
            {upsert: true}
        );
    },
    async findOne(isbn) {
        const books = await booksPromise;
        return await books.findOne(
            {isbn},
            { projection: {_id: 0} }
        );
    }
};
```

Dzięki tej zmianie bookController.js upraszcza się do
```javascript
const bookRepository = require("./bookRepository");

module.exports = {
    async createOrUpdate(req, res, next) {
        const {title, authors, isbn, description} = req.body;
        try {
            await bookRepository.createOrUpdate({title, authors, isbn, description});
            res.json({title, authors, isbn, description});
        } catch (e) {
            next(e);
        }
    },
    async details(req, res, next) {
        try {
            const isbn = req.params.isbn;
            const book = await bookRepository.findOne(isbn);
            res.json(book);
        } catch(e) {
            next(e);
        }
    }
};
```

### dodajemy testy e2e

Przetestujmy wszystko co zrobiliśmy do tej pory. Przy okazji chcemy lekko zmodyfikować odpowiedź z POST.
Zastosujemy wzorzec Post/Redirect/Get (https://en.wikipedia.org/wiki/Post/Redirect/Get). 

Nasz POST powinien odpowiedzieć kodem 302 i ustawić nagłówek Location do GET.
Przy okazji zmodyfikujmy testy aby używały async/await oraz promisów.  

```javascript
const assert = require("assert");

describe('Book inventory', function () {
    it('allows to stock up the items', async function () {
        const request =  httpClient(app);

        // CREATE
        const createResult = await request
            .post('/book')
            .send({
                title: "JavaScript in Action",
                authors: ["James Smith", "Kate Donovan"],
                isbn: "0123456789",
                description: "The ultimate JS book!"
            })
            .set('Content-Type', 'application/json')
            .expect(302);

        // READ
        const readResult = await request
            .get(createResult.header.location)
            .expect(200);

        assert.deepEqual(readResult.body, {
            title: "JavaScript in Action",
            authors: ["James Smith", "Kate Donovan"],
            isbn: "0123456789",
            description: "The ultimate JS book!"
        });
    })
});
```
Okazuje się, że funkcja expect z jednym argumentem zwraca Promise z odpowiedzią HTTP. 
await czeka na tę odpowiedź. Z odpowiedzi wyciągamy nagłówek location. 
Robimy zapytanie o szczegóły książki i oczekujemy dokładnie tego co stworzyliśmy krok wcześniej. 

Test nie działa, więc zaimplementujmy Post/Redirect/Get.

```javascript
res.redirect("/book/"+isbn);
``` 

### prosta logika biznesowa

Dostaliśmy teraz zadanie, aby dodać tzw. slug do każdej książki. Slug to tytuł przerobiony pod kątem łatwości zagnieżdżania
w URL. np. "java in action" staje się "java-in-action".
Pełna lista wymagań:
* slug zastępuje spacje przez myślniki
* slug usuwa wszystko co nie jest word character
* myślniki obok siebie zamieniają się w jeden myślnik
* na początku i końcu nie można dawać myślników

Udało nam się znaleźć przepis:
https://gist.github.com/mathewbyrne/1280286

Stwórz plik makeSlug.js i przetestuj czy znaleziony kod działa zgodnie z oczekiwaniami. W tym celu tworzymy test jednostkowy 
makeSlugTest.js

### service

Czyją odpowiedzialnością jest dodanie sluga do książki?
* repository nie powinno mieć takiej logiki
* controler powinien jedynie mapować świat HTTP and świat JS

Dodajmy zatem abstrakcję serwisu, który zbuduje sluga i wywoła zapis w repozytorium.

bookService.js
```javascript
const bookRepository = require("./bookRepository");
const makeSlug = require("./makeSlug");

module.exports = {
    async createOrUpdate({title, authors, isbn, description}) {
        const slug = makeSlug(title);
        return await bookRepository.createOrUpdate({title, slug, authors, isbn, description});
    }
};
```

bookController wywołuje teraz serwis zamiast repozytorium:
```
await bookService.createOrUpdate({title, authors, isbn, description})
```

Poprawny też odpowiednio test E2E

### walidacja

Możemy użyć bibliotek tj.:
* https://github.com/hapijs/joi
* https://github.com/ctavan/express-validator

Natomiast w tym ćwiczeniu napiszemy logikę walidacji w czystym JS.

Poniżej lista testów, które muszą być spełnione:
```javascript
const assert = require("assert");
const validateBook = require("../../src/validateBook");
const {string, stringArray} = require("./testData");

function basicBookWith(override = {}) {
    const validSpec = {title: "valid", authors: ["valid author"], isbn: "012345678A", description: "valid"};
    return Object.assign({}, validSpec, override);
}

describe("book validation", function () {
    it("valid title", function () {
        const error = validateBook(basicBookWith());

        assert.ok(!error);
    });

    it("empty title", function() {
        const error = validateBook(basicBookWith({title: "  "}));

        assert.deepStrictEqual(error, {titleError: "Please provide a title that is not empty"});
    });

    it("title too long", function() {
        const error = validateBook(basicBookWith({title: string(101)}));

        assert.deepStrictEqual(error, {titleError: "Please provide a title that is not longer than 100 characters"});
    });

    it("title not string", function() {
        const error = validateBook(basicBookWith({title: 1}));

        assert.deepStrictEqual(error, {titleError: "Please provide a title that is a string"});
    });

    it("no author", function () {
        const error = validateBook(basicBookWith({authors: []}));

        assert.deepStrictEqual(error, {authorsError: "Please provide at least one author"});
    });

    it("null author", function () {
        const error = validateBook(basicBookWith({authors: undefined}));

        assert.deepStrictEqual(error, {authorsError: "Please provide at least one author"});
    });

    it("too many authors", function () {
        const error = validateBook(basicBookWith({authors: stringArray(11, "author")}));

        assert.deepStrictEqual(error, {authorsError: "Please provide no more than 10 authors"});
    });

    it("empty author", function () {
        const error = validateBook(basicBookWith({authors: ["a", ""]}));

        assert.deepStrictEqual(error, {authorsError: "Author must not be empty"});
    });

    it("non-string author", function () {
        const error = validateBook(basicBookWith({authors: ["a", 1]}));

        assert.deepStrictEqual(error, {authorsError: "Author must be a string"});
    });

    it("author too long", function () {
        const error = validateBook(basicBookWith({authors: ["a", string(51)]}));

        assert.deepStrictEqual(error, {authorsError: "No author cannot be longer than 50 characters"});
    });

    it("description is optional", function () {
        const error = validateBook(basicBookWith({description: undefined}));

        assert.ok(!error);
    });

    it("description must be a string", function () {
        const error = validateBook(basicBookWith({description: 1}));

        assert.deepStrictEqual(error, {descriptionError: "Description must be a string"});
    });

    it("description too long", function () {
        const error = validateBook(basicBookWith({description: string(5001)}));

        assert.deepStrictEqual(error, {descriptionError: "Description cannot be longer than 5000 characters"});
    });

    it("isbn not string", function () {
        const error = validateBook(basicBookWith({isbn: 1234567890}));

        assert.deepStrictEqual(error, {isbnError: "Please make sure ISBN is 10 digits or upper-case letters"});
    });

    it("isbn too short", function () {
        const error = validateBook(basicBookWith({isbn: "123456789"}));

        assert.deepStrictEqual(error, {isbnError: "Please make sure ISBN is 10 digits or upper-case letters"});
    });

    it("isbn too long", function () {
        const error = validateBook(basicBookWith({isbn: "12345678900"}));

        assert.deepStrictEqual(error, {isbnError: "Please make sure ISBN is 10 digits or upper-case letters"});
    });

    it("isbn with lower case characters", function () {
        const error = validateBook(basicBookWith({isbn: "a234567890"}));

        assert.deepStrictEqual(error, {isbnError: "Please make sure ISBN is 10 digits or upper-case letters"});
    });
});
```
oraz pomocniczy plik testData.js
```javascript
function string(length) {
    const result = [];
    for(let i = 0; i < length; ++i) {
        result.push("a");
    }
    return result.join("");
}
function stringArray(times, string) {
    const result = [];
    for(let i = 0; i < times; ++i) {
        result.push(string + i);
    }
    return result;
}

module.exports = {string, stringArray};
```

### wpinamy walidację

Jeżeli potraktujemy walidację jako część logiki biznesowej możemy ją wpiąć do serwisu. 

```javascript
const bookRepository = require("./bookRepository");
const makeSlug = require("./makeSlug");
const validateBook = require("./validateBook");

module.exports = {
    async createOrUpdate({title, authors, isbn, description}) {
        const validationErrors = validateBook({title, authors, isbn, description});
        if(!validationErrors) {
            const slug = makeSlug(title);
            await bookRepository.createOrUpdate({title, slug, authors, isbn, description});
        } else {
            return validationErrors;
        }
    }
};
```
Nieprawidłowe dane zwracamy jako obiekt z błędami, w przypadku sukcesu nie zwracamy nic.

I w bookController.js
```
 async createOrUpdate(req, res, next) {
        const {title, authors, isbn, description} = req.body;
        try {
            const validationErrors = await bookService.createOrUpdate({title, authors, isbn, description});
            if(!validationErrors) {
                res.redirect("/book/"+isbn);
            } else {
                const error = new Error();
                error.message = validationErrors;
                error.status = 400;
                next(error);
            }
        } catch (e) {
            next(e);
        }
    }
```

W tym kroku zabezpieczyliśmy się przed złośliwymi danymi. 
Dodatkowo możemy użyć narzędzia http://www.zaproxy.org/ aby uruchomić tzw. fuzz testy i zidentyfikować
te przypadki, gdzie zapomnieliśmy dodać odpowiednią walidację. 

### delete

Mamy już create, read, update, czas na delete.

bookRoutes.js
```javascript
router.delete("/:isbn", controller.delete);
```
bookController.js
```
async delete(req, res, next) {
    try {
        const isbn = req.params.isbn;
        
        await bookRepository.delete(isbn);
        
        res.status(204).end();
    } catch (e) {
        next(e);
    }
}
```
Odsyłamy 204 - No Content. 

bookRepository.js
```
async delete(isbn) {
    const books = await booksPromise;
    return await books.deleteOne({isbn});
}
```

Przetestuj kasowanie

### read dla skasowanej książki

Testując delete okazało się, że zwracamy 200 OK z nullem.
Zamiast tego chcemy zwrócić 404 - Not Found.

W bookController.js details:
```
if(book) {
   res.json(book);
} else {
   next();
}
```

Korzystamy z tego, że middleware 404 jest wpięty za naszymi handlerami. Przekazujemy do niego kontrolę
wywołując next() bez argumentów. Wywołanie z argumentem spowoduje odpalenia ostatniego error handlera
z 4 parametrami w sygnaturze.  

### CRUD test - pełny cykl

Przetestujmy teraz E2E cały cykl Create-Read-Update-Delete

### usuwanie duplikacji

Zanim zaczniemy dodawać kolejne funkcjonalności do systemu, usuńmy duplikację. 
W bookController.js każda funkcja powtarza blok try/catch. 
W jaki sposób możemy uniknąc duplikacji kodu?